### Islanded Particle Swarm Optimization in CUDA ###

A first attempt from a long time ago. This is a multi-population PSO algorithm with each population (or neighborhood) being assigned to a single block.

## Getting started ##
To get started, type "make all" in the default directory. 