#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>

#include <cuda.h>
#include <curand_kernel.h>
#include "cuPrintf.cu"
#define CUDART_INF_F __int_as_float(0x7f800000)

#include "hr_time.h"
#include "my_cutil.h"
#include "MersenneTwister.h"

using namespace std;
#define PI_F 3.141592654f 

typedef vector< long double > ldArray1D;
typedef vector< vector< long double > > ldArray2D;
typedef vector< vector< vector <long double > > > ldArray3D;


double sigMoid(double v){

	return 1/(1+exp(-v));
}

long double F1(ldArray2D& R, int Nd, int p) { // Sphere
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Z += Xi*Xi;
	}
	return -Z;

}

long double F2(ldArray2D& R, int Nd, int p) {  
	long double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Z += (pow(Xi,2) - 10 * cos(2*PI_F*Xi) + 10);
	}
	return -Z;
}
long double F3(ldArray2D& R, int Nd, int p) {
	long double Z, Sum, Prod, Xi;

    Z = 0; Sum = 0; Prod = 1;
    
	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Sum  += Xi*Xi;
		Prod *= cos(Xi/sqrt((double)i)+1)/4000.0f; 
		
		if(isnan(Prod)) Prod = 1;
    }
	
	Z = Sum - Prod;
	
	return -Z;
}
long double F4(ldArray2D& R, int Nd, int p) {
	long double Z=0, Xi, XiPlus1;

	for(int i=0; i<Nd-1; i++){
		Xi = R[p][i];
		XiPlus1 = R[p][i+1];
		Z = Z + (100*(XiPlus1-Xi*Xi)*(XiPlus1-Xi*Xi) + (Xi-1)*(Xi-1));
	}
	return -Z;
}

// Nn Blocks, Np Threads
__global__ void evalFitness(float* R, float* M, int Np, int Nd){
    float Mp;
    int   p  = threadIdx.x + blockIdx.x * blockDim.x;
     
    Mp = 0;
    for(int i=0; i<Nd; i++){
        Mp += (R[p*Nd+i]*R[p*Nd+i]);
    }   
    M[p] = -Mp;
}

/******************************************************* Begin Initializiation *******************************************************/
__global__ void setup_kernel(curandState *state){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(clock(), id, 0, &state[id]);
}

__global__ void initPop(float* R, float* V, float* M, float* pBestValue, float* pBestPosition, int Np, int Nd, int xMin, int xMax, int vMin, int vMax, curandState *state){
    int p = threadIdx.x + blockIdx.x * blockDim.x;
    M[p]          = -CUDART_INF_F;
    pBestValue[p] = -CUDART_INF_F;
    
    for(int i=0; i<Nd; i++){        
        R[p*Nd+i] = xMin + curand_uniform(&state[p])*(xMax-xMin);
        V[p*Nd+i] = vMin + curand_uniform(&state[p])*(vMax-vMin);
        if(curand_uniform(&state[p]) < 0.5){
        	R[p*Nd+i] = -R[p*Nd+i];
	        V[p*Nd+i] = -V[p*Nd+i];
        }
        pBestPosition[p*Nd+i] = 0;

    }
}
__global__ void init_Nn(float* gBestValue, float* gBestPosition, int Nd){
    int n = blockIdx.x;
    
    gBestValue[n] = -CUDART_INF_F;
    for(int i=0; i<Nd; i++){
        gBestPosition[n*Nd+i] = 0;
    }
}
/******************************************************* End Initializiation *******************************************************/

__global__ void updateLocalBest(float* R, float* M, float* pBestValue, float* pBestPosition, int Nd){
    int p = threadIdx.x + blockIdx.x * blockDim.x;
    
    if(M[p] > pBestValue[p]){
        pBestValue[p] = M[p];
        for(int i=0; i<Nd; i++){
            pBestPosition[p*Nd+i] = R[p*Nd+i];
        }
    }
}

// Done on a per block basis now
// Should be a reduction over a single block
__global__ void updateGlobalBest(float* R, float* M, float* gBestValue, float* gBestPosition, int Np, int Nd){
    int n     = blockIdx.x;  // Neighborhood
    int start = blockIdx.x * Np;
    int end   = start + Np;
    
    for(int p = start; p<end; p++){
        if(M[p] > gBestValue[n]){
            gBestValue[n] = M[p];
            
            for(int i=0; i<Nd; i++){
                gBestPosition[n*Nd+i] = R[p*Nd+i];
            }
        }
    }
}
// Shared Memory = sizeof(float)*Np + sizeof(int)+Np
__global__ void updateGlobalBest2(float* R, float* M, float* gBestValue, float* gBestPosition, int Np, int Nd){

    extern __shared__ float bestValue[];
    extern __shared__ int   bestIndex[];
    int tid = threadIdx.x;
    int n   = blockIdx.x;
    int p   = blockIdx.x*blockDim.x + threadIdx.x;
    
    bestValue[tid] = M[p];
    bestIndex[tid] = tid;
    
    __syncthreads();
    
    cuPrintf("%f %f %f\n", bestValue[tid], M[p],  gBestValue[n]);
    
    __syncthreads();

    // do reduction in shared mem
    for(unsigned int s=blockDim.x/2; s>0; s>>=1) {
        if (tid < s){
            if(bestValue[tid] < bestValue[tid + s]){
                bestValue[tid] = bestValue[tid + s];
                bestIndex[tid] = tid+s;
            }
        }
        __syncthreads();
    }
    __syncthreads();
    
    //cuPrintf("%f", bestValue[tid]);
    
    // write result for this block to global mem
    if(tid == 0){
        gBestValue[n] = bestValue[0];
        for(int i=0; i<Nd; i++){
            gBestPosition[n*Nd+i] = R[bestIndex[tid]*Nd+i];
        }
    }
}

__global__ void allInOne(float* R, float* V, float* M, float* pBestValue, float* pBestPosition, 
                    float* gBestValue, float* gBestPosition, 
                    int Np, int Nd, int Nt, int xMin, int xMax, int vMin, int vMax,
                    float wMax, float wMin, float C1, float C2, curandState *state){
                    
    int n, p, globalP, localP;
    curandState localState;
    float pos, Mp, Vel, w;
    extern __shared__ float _R[], _V[], _M[], sdata[];
    extern __shared__ float _pBestValue[], _pBestPosition[], _gBestPosition[];
    __shared__ float _gBestValue;
    
    
    
    n       = blockIdx.x; 
    globalP = threadIdx.x + blockIdx.x * blockDim.x;
    p       = threadIdx.x;
    localState = state[globalP];
    
    if(p == 0){
        _gBestValue = -CUDART_INF_F;
    }
    // Copy to Shared Memory

    _M[p] = M[globalP];
    for(int _i=0; _i<Nd;_i++){
        _R[p*Nd+_i] = R[globalP*Nd+_i];
        _V[p*Nd+_i] = V[globalP*Nd+_i];
    }
    
    for(int j=0; j< Nt; j++){
    
        // Update Positions
        for(int i=0; i<Nd; i++){
            pos = _R[p*Nd+i] + _V[p*Nd+i];
            if(pos > xMax) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
            if(pos < xMin) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
            _R[p*Nd+i] = pos;    
        }
        __syncthreads();
        
        // Calculate Fitness     
        _M[p] = 0;
        for(int i=0; i<Nd; i++){
            Mp += (_R[p*Nd+i]*_R[p*Nd+i]);
        }   
        _M[p] = -Mp;
        __syncthreads();
        
        // Local Best
        if(_M[p] > _pBestValue[p]){
            _pBestValue[p] = _M[p];
            for(int i=0; i<Nd; i++){
                _pBestPosition[p*Nd+i] = _R[p*Nd+i];
            }
        }
        __syncthreads();
        
        // Global Best   
        sdata[p] = _M[p];
        
        __syncthreads();
        for(int s=1; s<blockDim.x; s*=2){
            if(p % (2*s) == 0){
                if(sdata[p+s] > sdata[p]){
                    sdata[p] = sdata[p+s];
                    
                    //Copy position
                    localP = (p+s)+ blockIdx.x * blockDim.x;
                    for(int i=0; i<Nd; i++){
                        gBestPosition[n*Nd+i] = _R[localP*Nd+i];
                    }
                }
            }
            __syncthreads();
        }
        __syncthreads();
        if(p == 0){
            _gBestValue = sdata[0];
        }
        __syncthreads();
        
        // Velocity
        w = wMax - ((wMax-wMin)/Nt) * j;
        for(int i=0; i<Nd; i++){
            Vel = _V[(p*Nd+i)];
            Vel = w * Vel;
            Vel += C1*curand_uniform(&localState)*(pBestPosition[p*Nd+i] - R[p*Nd+i]);
            Vel += C2*curand_uniform(&localState)*(gBestPosition[n*Nd+i] - R[p*Nd+i]);      
            
            if(Vel > vMax) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);
            if(Vel < vMin) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);  
            V[p*Nd+i] = Vel; 
        }
        __syncthreads();
    }
    //Copy to Global Memory
    gBestValue[n] = _gBestValue;
}
__global__ void updateR(float* R, float* V, curandState *state, int Np, int Nd, float xMin, float xMax){
    int p  = threadIdx.x + blockIdx.x * blockDim.x;
    curandState localState = state[p];
    float pos;
        
    for(int i=0; i<Nd; i++){
        pos = R[p*Nd+i] + V[p*Nd+i];
        if(pos > xMax) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
        if(pos < xMin) pos = xMin + curand_uniform(&localState)*(xMax-xMin);
        R[p*Nd+i] = pos;    
    }
}
__global__ void updateV(float* V, float* R, float* pBestPosition, float* gBestPosition, curandState *state, float chi, int Np, int Nd, int vMin, int vMax, int C1, int C2){
    int n  = blockIdx.x;  // Neighborhood
    int p  = threadIdx.x + blockIdx.x * blockDim.x; // Overall Particle Number
    
    float Vel;
    curandState localState = state[p];    
    
    for(int i=0; i<Nd; i++){
        Vel = V[(p*Nd+i)];
        Vel = chi * Vel;
        Vel += C1*curand_uniform(&localState)*(pBestPosition[p*Nd+i] - R[p*Nd+i]);
        Vel += C2*curand_uniform(&localState)*(gBestPosition[n*Nd+i] - R[p*Nd+i]);      
        
        if(Vel > vMax) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);
        if(Vel < vMin) Vel = vMin + curand_uniform(&localState)*(vMax-vMin);  
        V[p*Nd+i] = Vel; 
    }
    state[p] = localState;
  
}

__global__ void sort(float *R, float *V, float *M, float *pBestPosition, float *pBestValue, int Np, int Nd){
    int n = blockIdx.x;
    int start = threadIdx.x + blockIdx.x * blockDim.x;
    int end   = start + Np;
    float fTemp;
    
    for(int pass=start; pass<end-1; pass++) {
        int smallest = pass;  
        
        for(int i=pass+1; i<end; i++) {
            if (M[i] < M[smallest]) {
                smallest = i;
            }
        }

        fTemp                = M[pass];
        M[pass]              = M[smallest];
        M[smallest] = fTemp;
        
        fTemp           = pBestValue[pass];
        pBestValue[pass] = pBestValue[smallest];
        pBestValue[smallest] = fTemp;
        
        for(int i=0; i<Nd; i++){
            fTemp = R[pass*Nd+i];
            R[pass*Nd+i] = R[smallest*Nd+i];
            R[smallest*Nd+i] = fTemp;
            
            fTemp = V[pass*Nd+i];
            V[pass*Nd+i] = V[smallest*Nd+i];
            V[smallest*Nd+i] = fTemp;
            
            fTemp = pBestPosition[pass*Nd+i];
            pBestPosition[pass*Nd+i] = pBestPosition[smallest*Nd+i];
            pBestPosition[smallest*Nd+i] = fTemp;
        }
    }
}
 
__global__ void migrate(float *R, float *V, float *M, float *pBestPosition, float *pBestValue, int Np, int Nd){
    int n       = blockIdx.x;
    int to      = n+1;
    int startHere, startThere;
    
    if(to > blockDim.x){ 
        to=0;
    }

    // My 2 best to their 2 worst
    startHere  = n*Np+Np-2;
    startThere = to*Np;   
    for(int p=startHere; p<startHere+2; p++, startThere++){
        pBestValue[startThere] = pBestValue[p];
        M[startThere]          = M[p];    
        for(int i=0; i<Nd; i++){
            pBestPosition[startThere*Nd+i] = pBestPosition[p*Nd+i];
            R[startThere*Nd+i] = R[p*Nd+i];
            V[startThere*Nd+i] = V[p*Nd+i];
        }
    }    
}

// Np - Pop per neighborhood



void PSO2(int Np, int Nd, int Nt, int Nn, long double xMin, long double xMax, long double vMin, long double vMax, 
         long double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){

    if(cudaPrintfInit() != cudaSuccess){
        cout << "cuPrintf Error\n";
    }
    
    float C1, C2, bestGbest;
	float w, wMax, wMin;
    int lastStep;
	
	CStopWatch timer, timer1;
	long double positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0, initTime = 0;
		
	float *R_GPU, *V_GPU, *M_GPU,
	      *pBestPosition_GPU, *gBestPosition_GPU, 
	      *pBestValue_GPU,    *gBestValue_GPU,
	      *gBestValue;
	
    if(Np >= 512){ Np = 512;}

	C1 = 2.05; C2 = 2.05;
	wMin = 0.4; wMax = 0.9;
	lastStep = Nt;
    numEvals = 0;
    positionTime = 0; fitnessTime = 0; velocityTime = 0; totalTime = 0;

    bestGbest = -INFINITY;
    gBestValue = new float[Nn];
    for(int i=0; i<Nn; i++){
    	gBestValue[i] = -INFINITY;
    }
	// Allocate on GPU
    CudaSafeCall(cudaMalloc((void**) &V_GPU,             Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &R_GPU,             Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &M_GPU,             Nn*Np   *sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestPosition_GPU, Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestValue_GPU,    Nn*Np*   sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &gBestPosition_GPU, Nn*Nd   *sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &gBestValue_GPU,    Nn      *sizeof(float)));

    timer1.startTimer();

    curandState *devStates;
    CudaSafeCall(cudaMalloc((void **) &devStates, Nn*Np*Nd*sizeof(curandState)));

    setup_kernel<<< Nn, Np >>> (devStates); CudaCheckError();
    initPop     <<< Nn, Np >>> (R_GPU, V_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, Np, Nd, xMin, xMax, vMin, vMax, devStates); CudaCheckError();    
    init_Nn     <<< Nn, 1  >>> (gBestValue_GPU, gBestPosition_GPU, Nd); CudaCheckError();
    timer1.stopTimer();
    initTime += timer1.getElapsedTime();
    
    evalFitness <<< Nn, Np >>>(R_GPU, M_GPU, Np, Nd); CudaCheckError();
    numEvals += (Np*Nn); 

    double reqMem = 3*Np*Nd*sizeof(float);
    reqMem += 2*Np*sizeof(float);
    reqMem += Nd*sizeof(float);
    reqMem += sizeof(float);
    allInOne <<<Nn, Np, reqMem >>>(R_GPU, V_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, gBestValue_GPU, gBestPosition_GPU, 
                    Np, Nd, Nt, xMin, xMax, vMin, vMax,
                    wMax, wMin, C1, C2, devStates);
                    
    CudaSafeCall(cudaMemcpy(gBestValue, gBestValue_GPU, Nn*sizeof(float), cudaMemcpyDeviceToHost));
    for(int i=0; i<Nn; i++){
        cout << gBestValue[i] << " ";
    }
    cout << endl;                   
                    
                    
    cin.ignore(1,'\n');

    cudaPrintfEnd();
    CudaSafeCall(cudaFree(pBestPosition_GPU));
    CudaSafeCall(cudaFree(pBestValue_GPU));
    
    CudaSafeCall(cudaFree(gBestPosition_GPU));
    CudaSafeCall(cudaFree(gBestValue_GPU));
    
    CudaSafeCall(cudaFree(R_GPU));
    CudaSafeCall(cudaFree(V_GPU));
    CudaSafeCall(cudaFree(M_GPU));
    
    CudaSafeCall(cudaFree(devStates));
        
	cout    << functionName << " "
            << bestGbest    << " " 
            << Np           << " "
            << Nd           << " "
            << Nn           << " "
            << lastStep     << " "
            << numEvals     << " "
            << positionTime << " "
            << fitnessTime  << " "
            << velocityTime << " "
            << totalTime    << endl;
}
void PSO(int Np, int Nd, int Nt, int Nn, long double xMin, long double xMax, long double vMin, long double vMax, 
         long double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){

    if(cudaPrintfInit() != cudaSuccess){
        cout << "cuPrintf Error\n";
    }
    
    float C1, C2, bestGbest;
	float w, wMax, wMin;
    int lastStep;
	
	CStopWatch timer, timer1;
	long double positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0, initTime = 0;
		
	float *R_GPU, *V_GPU, *M_GPU,
	      *pBestPosition_GPU, *gBestPosition_GPU, 
	      *pBestValue_GPU,    *gBestValue_GPU,
	      *gBestValue;
	
    if(Np >= 512){ Np = 512;}

	C1 = 2.05; C2 = 2.05;
	wMin = 0.4; wMax = 0.9;
	lastStep = Nt;
    numEvals = 0;
    positionTime = 0; fitnessTime = 0; velocityTime = 0; totalTime = 0;

    bestGbest = -INFINITY;
    gBestValue = new float[Nn];
    for(int i=0; i<Nn; i++){
    	gBestValue[i] = -INFINITY;
    }
	// Allocate on GPU
    CudaSafeCall(cudaMalloc((void**) &V_GPU,             Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &R_GPU,             Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &M_GPU,             Nn*Np   *sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestPosition_GPU, Nn*Np*Nd*sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &pBestValue_GPU,    Nn*Np*   sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &gBestPosition_GPU, Nn*Nd   *sizeof(float)));
    CudaSafeCall(cudaMalloc((void**) &gBestValue_GPU,    Nn      *sizeof(float)));

    timer1.startTimer();

    curandState *devStates;
    CudaSafeCall(cudaMalloc((void **) &devStates, Nn*Np*Nd*sizeof(curandState)));

    
    setup_kernel<<< Nn, Np >>> (devStates); CudaCheckError();
    initPop     <<< Nn, Np >>> (R_GPU, V_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, Np, Nd, xMin, xMax, vMin, vMax, devStates); CudaCheckError();    
    init_Nn     <<< Nn, 1  >>> (gBestValue_GPU, gBestPosition_GPU, Nd); CudaCheckError();
    timer1.stopTimer();
    initTime += timer1.getElapsedTime();
    
    evalFitness <<< Nn, Np >>> (R_GPU, M_GPU, Np, Nd); CudaCheckError();
    numEvals += (Np*Nn); 

    for(int j=1; j<Nt; j++){
    
		timer.startTimer();
        updateR <<< Nn, Np >>> (R_GPU, V_GPU, devStates, Np,  Nd, xMin, xMax); CudaCheckError();
		timer.stopTimer();
		positionTime += timer.getElapsedTime();

        if(j > 0 && j % 50 == 0){
		    sort    <<< Nn, 1 >>> (R_GPU, V_GPU, M_GPU, pBestPosition_GPU, pBestValue_GPU, Np, Nd);
		    migrate <<< Nn, 1 >>> (R_GPU, V_GPU, M_GPU, pBestPosition_GPU, pBestValue_GPU, Np, Nd);
		}
		
		timer.startTimer();
		evalFitness      <<< Nn, Np >>> (R_GPU, M_GPU, Np, Nd); CudaCheckError();
		
		// TODO: Naive. Move to Reducton
        updateGlobalBest <<< Nn, 1  >>> (R_GPU, M_GPU, gBestValue_GPU, gBestPosition_GPU, Np, Nd); CudaCheckError();
        updateLocalBest   <<< Nn, Np >>> (R_GPU, M_GPU, pBestValue_GPU, pBestPosition_GPU, Nd);     CudaCheckError();
    
        CudaSafeCall(cudaMemcpy(gBestValue, gBestValue_GPU, Nn*sizeof(float), cudaMemcpyDeviceToHost));
        numEvals += (Np*Nn);
        timer.stopTimer();
		fitnessTime += timer.getElapsedTime();
//        cout << j << " "; 
        for(int n=0; n<Nn; n++){
//            cout << gBestValue[n] << " ";
            if(gBestValue[n] > bestGbest){
                bestGbest = gBestValue[n];
            }
	    }
//	    cout << endl;
	    if(bestGbest >= -0.0001){
            lastStep = j;
    	    break;
	    }

        // Update Velocities
		timer.startTimer();
		w = wMax - ((wMax-wMin)/Nt) * j;
		
        updateV <<< Nn, Np >>> (V_GPU, R_GPU, pBestPosition_GPU, gBestPosition_GPU, devStates, w, Np, Nd, vMin, vMax, C1, C2); CudaCheckError();
        timer.stopTimer();
		velocityTime += timer.getElapsedTime();
	} // End Time Steps
    timer1.stopTimer();
//    cin.ignore(1,'\n');
    totalTime += timer1.getElapsedTime();

    cudaPrintfEnd();
    CudaSafeCall(cudaFree(pBestPosition_GPU));
    CudaSafeCall(cudaFree(pBestValue_GPU));
    
    CudaSafeCall(cudaFree(gBestPosition_GPU));
    CudaSafeCall(cudaFree(gBestValue_GPU));
    
    CudaSafeCall(cudaFree(R_GPU));
    CudaSafeCall(cudaFree(V_GPU));
    CudaSafeCall(cudaFree(M_GPU));
    
    CudaSafeCall(cudaFree(devStates));
        
	cout    << functionName << " "
            << bestGbest    << " " 
            << Np           << " "
            << Nd           << " "
            << Nn           << " "
            << lastStep     << " "
            << numEvals     << " "
            << positionTime << " "
            << fitnessTime  << " "
            << velocityTime << " "
            << totalTime    << endl;
}

void run_PSO(long double xMin, long double xMax, long double vMin, long double vMax, long double (*rPtr)(ldArray2D& , int, int), string functionName){

    int Nd, NdMin, NdMax, NdStep,
        Np, NpMin, NpMax, NpStep,
        Nn, NnMin, NnMax, NnStep,
        Nt;
    int numEvals,  deviceCount, dev;
    cudaDeviceProp deviceProp;

    NpMin = 4;  NpMax = 128;  NpStep = 2; 
    NdMin = 8;  NdMax = 4096; NdStep = 2;
    NnMin = 1;  NnMax = 4096;  NnStep = 2;
   
   int counter = 0;

//    Nt = 1000; Np = 32; Nd = 32; Nn = 256;
//    PSO(32, 32, Nt, 256, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
    Nt = 1000;
    Np = 128; Nn = 64;
    //for(Np=NpMin; Np<=NpMax; Np*=NpStep){
        for(Nd=NdMin; Nd<=NdMax; Nd*=NdStep){
            //for(Nn=NnMin; Nn<=NnMax; Nn*=NnStep){
                for(int x=0; x<10; x++){
                    cudaGetDeviceCount(&deviceCount);
                    cudaGetDeviceProperties(&deviceProp, dev);
                    //cudaSetDevice(counter%deviceCount);
                    cudaSetDevice(0);
                    counter++;
                    PSO(Np, Nd, Nt, Nn, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
                    cudaThreadExit();
                }
            //}
        }
    //}
}
int main(){

    int deviceCount=0, dev=0;
	long double (*rPtr)(ldArray2D& , int, int) = NULL;
	long double xMin, xMax, vMin, vMax;
    cudaDeviceProp deviceProp;
    
    cudaGetDeviceCount(&deviceCount);
    cudaGetDeviceProperties(&deviceProp, dev);
    
    cout << "Function, Fitness, Np, Nd, Nn, Last Step, Evals, Position Time, Fitness Time, Velocity Time, Total Time" << endl;
    
    rPtr = &F1;
    xMin = -100; xMax = 100;
    vMin = -100; vMax = 100;
    run_PSO(xMin, xMax, vMin, vMax, rPtr, "F1");
    
//    rPtr = &F2; Np = 120;
//    xMin = -10; xMax = 10;
//    vMin = -10; vMax = 10;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F2");
//    
//    rPtr = &F3; Np = 180;
//    xMin = -600; xMax = 600;
//    vMin = -600; vMax = 600;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F3");

//    rPtr = &F4; Np = 180;
//    xMin = -10; xMax = 10;
//    vMin = -10; vMax = 10;
//    run_PSO(Np, xMin, xMax, vMin, vMax, rPtr, "F4");

    rPtr = NULL;

	return 0;
}
