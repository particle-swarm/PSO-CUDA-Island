#include <iostream>
#include <string>
#include <vector>

#include <curand_kernel.h>
#include <cuda.h>
#include "hr_time.h"
#include "MersenneTwister.h"

using namespace std;

typedef vector<int> 		iArray1D;
typedef vector<long double> ldArray1D;
typedef vector<ldArray1D> 	ldArray2D;
typedef vector<ldArray2D> 	ldArray3D;

__global__ void setup_kernel(curandState *state, int seed){
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(clock(), id, 0, &state[id]);
}

__global__ void calculateV(float* V, float* R, float* pBestPosition, float* gBestPosition, curandState *state, float chi, int Nd, int vMin, int vMax, int C1, int C2){
    int p  = blockIdx.x;  // Current Block
    int i  = threadIdx.x; // Current Thread
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    float Vel = V[p*Nd+i];
    curandState localState = state[id];
    
    // Use of Local Memory
    float R1 = curand_uniform(&localState);
    float R2 = curand_uniform(&localState);
    float R3 = curand_uniform(&localState);
    float R4 = curand_uniform(&localState);

    // Original PSO
    Vel = chi * Vel + C1*R1*(pBestPosition[p*Nd+i] - R[p*Nd+i]) + C2*R2*(gBestPosition[i] - R[p*Nd+i]);              

    if(Vel > vMax) Vel = vMin + R3*(vMax-vMin);
    if(Vel < vMin) Vel = vMin + R4*(vMax-vMin);
    
    V[p*Nd+i] = Vel;

}

__global__ void updateR(float* R, float* V, curandState *state, int Nd, float xMin, float xMax){
    int p  = blockIdx.x;  // Current Block
    int i  = threadIdx.x; // Current Thread
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curandState localState = state[id];
    
    float pos = R[p*Nd+i];
    float R1 = curand_uniform(&localState);
    float R2 = curand_uniform(&localState);

    // Original PSO
    pos = pos + V[p*Nd+i];

	if(pos > xMax) pos = xMin + R1*(xMax-xMin);
    if(pos < xMin) pos = xMin + R1*(xMax-xMin);
    
    R[p*Nd+i] = pos;

}

extern "C" void PSO(int Np, int Nd, int Nt, long double xMin, long double xMax, long double vMin, long double vMax,long double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){

	vector < vector < long double > > R(Np, vector < long double >(Nd,0));
	vector < vector < long double > > V(Np, vector < long double >(Nd,0));
    vector < long double > M(Np,-INFINITY);

	ldArray2D pBestPosition(Np, vector<long double>(Nd,-INFINITY));
	ldArray1D pBestValue(Np,-INFINITY);
	
	ldArray1D gBestPosition(Nd, -INFINITY);
    long double gBestValue = -INFINITY;
    
    int lastStep = Nt;
	
	long double  C1  = 2.05, C2 = 2.05;
	long double  phi = C1 + C2;
	long double  chi = 0.95;
    MTRand mt;
    
	CStopWatch timer, timer1;
	long double positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0;;
		
	float   *R_Linear, *V_Linear, *pBestPosition_Linear, *gBestPosition_Linear,
	        *R_GPU, *V_GPU, *pBestPosition_GPU, *gBestPosition_GPU;
	
	
	
	R_Linear = new float[Np*Nd]; 
	V_Linear = new float[Np*Nd];
	pBestPosition_Linear = new float[Np*Nd];
	gBestPosition_Linear = new float[Nd];
	
	numEvals = 0;
    timer1.startTimer();

    // Set up RNG
    curandState *devStates;
    cudaMalloc((void **)&devStates, Np*Nd*sizeof(curandState));
    setup_kernel<<<Np, Nd>>>(devStates, time(NULL));
    
    // End RNG
	// Init Population
	for(int p=0; p<Np; p++){
		for(int i=0; i<Nd; i++){
			R[p][i] = xMin + mt.randDblExc(xMax-xMin);
			V[p][i] = vMin + mt.randDblExc(vMax-vMin);

			if(mt.rand() < 0.5){
				R[p][i] = -R[p][i];
				V[p][i] = -V[p][i];
			}
		}
	}

	// Evaluate Fitness
	for(int p=0; p<Np; p++){
		 M[p] = objFunc(R, Nd, p);
		 numEvals++;
	}

	//for(int j=1; j<Nt; j++){
    for(int j=1; j<Nt; j++){

		//Update Positions
		timer.startTimer();
		for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                V_Linear[p*Nd+i] = V[p][i];   
                R_Linear[p*Nd+i] = R[p][i];
            }
        }
        
        cudaMalloc((void**) &V_GPU, Np*Nd*sizeof(float)); 
        cudaMalloc((void**) &R_GPU, Np*Nd*sizeof(float));
        cudaMemcpy(V_GPU, V_Linear, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(R_GPU, R_Linear, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        updateR<<<Np, Nd>>>(R_GPU, V_GPU, devStates, Nd, xMin, xMax);
        cudaMemcpy(R_Linear, R_GPU, Np*Nd*sizeof(float), cudaMemcpyDeviceToHost);

        for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                R[p][i] = R_Linear[p*Nd+i];   
            }
        }       
        cudaFree(R_GPU);cudaFree(V_GPU);
		timer.stopTimer();
		positionTime += timer.getElapsedTime();

		// Evaluate Fitness
		timer.startTimer();
		for(int p=0; p<Np; p++){
			M[p] = objFunc(R, Nd, p);
			numEvals++;
		}
		
		for(int p=0; p<Np; p++){		    
            if(M[p] > gBestValue){
				gBestValue = M[p];
				for(int i=0; i<Nd; i++){
				    gBestPosition[i] = R[p][i];
				}
			}
			
			// Local
		    if(M[p] > pBestValue[p]){
			    pBestValue[p] = M[p];
                for(int i=0; i<Nd; i++){
				    pBestPosition[p][i] = R[p][i];
				}
			}
		}
		timer.stopTimer();
		fitnessTime += timer.getElapsedTime();
        
        if(gBestValue >= -0.0001){
            lastStep = j;
    	    break;
	    }
	    
        for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                V_Linear[p*Nd+i] = V[p][i];   
                R_Linear[p*Nd+i] = R[p][i];   
                pBestPosition_Linear[p*Nd+i] = pBestPosition[p][i];
            }
        }
        for(int i=0; i<Nd; i++){
            gBestPosition_Linear[i] = gBestPosition[i];
        }        

        // Update Velocities
		timer.startTimer();
        cudaMalloc((void**) &V_GPU, Np*Nd*sizeof(float));
        cudaMalloc((void**) &R_GPU, Np*Nd*sizeof(float));
        cudaMalloc((void**) &pBestPosition_GPU, Np*Nd*sizeof(float));
        cudaMalloc((void**) &gBestPosition_GPU, Nd*sizeof(float));
        
        cudaMemcpy(V_GPU, V_Linear, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(R_GPU, R_Linear, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(pBestPosition_GPU, pBestPosition_Linear, Np*Nd*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(gBestPosition_GPU, gBestPosition_Linear, Nd*sizeof(float), cudaMemcpyHostToDevice);
        
        calculateV<<<Np, Nd>>>(V_GPU, R_GPU, pBestPosition_GPU, gBestPosition_GPU, devStates, chi, Nd, vMin, vMax, C1, C2);
        
        cudaMemcpy(V_Linear, V_GPU, Np*Nd*sizeof(float), cudaMemcpyDeviceToHost);
        for(int p=0; p<Np; p++){
            for(int i=0; i<Nd; i++){
                V[p][i] = V_Linear[p*Nd+i];  
            }
        }
        cudaFree(pBestPosition_GPU);
        cudaFree(gBestPosition_GPU);
        //cudaFree(rand_GPU); 
        cudaFree(R_GPU);
        cudaFree(V_GPU);
      
        timer.stopTimer();
		velocityTime += timer.getElapsedTime();
	} // End Time Steps

    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();

    R.clear(); V.clear(); M.clear();
    pBestPosition.clear(); pBestValue.clear(); gBestPosition.clear();

    //cout << setw(23) << left << "Function Name:" << functionName << endl;
//	cout << setw(23) << left << "Best Fitness:" << setprecision(10) << gBestValue << endl;
//	cout << setw(23) << left << "Number of Probes:" << Np << endl;
//	cout << setw(23) << left << "Fitness:" << setprecision(4) << gBestValue	 << endl;
//    cout << setw(23) << left << "Evaluations:" 	<< setprecision(4) << numEvals 	 << endl;
//	cout << "-------------------------- Times --------------------------" 			 << endl;
//	cout << setw(23) << left << "Position:" << setprecision(4) << positionTime 	 << endl;
//	cout << setw(23) << left << "Fitness:" 	<< setprecision(4) << fitnessTime << endl;
//	cout << setw(23) << left << "Velocity:" << setprecision(4) << velocityTime	 << endl;
//    cout << setw(23) << left << "Total:" 	<< setprecision(4) << totalTime 	 << endl;
        
	cout    << functionName << ","
            << gBestValue  << "," 
            << Np << ","
            << Nd << ","
            << lastStep << ","
            << numEvals << ","
            << positionTime << ","
            << fitnessTime << ","
            << velocityTime << ","
            << totalTime << endl;
}
